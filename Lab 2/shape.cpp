#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

    Point::Point(const Point &point) {
        _x = point._x;
        _y = point._y;
    }

    double Ellipse::Area() const {
        return M_PI * _a * _b;
    }

    Circle::Circle(const Point &center, const int &radius) : Ellipse(center, radius, radius) {
    }

    Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3) {

        a = Point::distance(p1, p2);
        b = Point::distance(p2, p3);
        c = Point::distance(p3, p1);
        s = 0.5 * (a + b + c);
    }

    Triangle::Triangle(double a) {
        this->a = a;
        this->b = a;
        this->c = a;
        s = 0.5 * (3 * a);
    }

    double Triangle::Area() const {
        return sqrt(s * (s - a) * (s - b) * (s - c));
    }

    TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge) : Triangle(edge) {
    }


    Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4) {

        d1 = Point::distance(p1, p2);
        d2 = Point::distance(p2, p3);
        d3 = Point::distance(p3, p4);
        d4 = Point::distance(p4, p1);
        p = 0.5 * (d1 + d2 + d3 + d4);
    }

    Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3) {
        d1 = Point::distance(p1, p2);
        d2 = Point::distance(p2, p3);
        d3 = d1;
        d4 = d2;
        p = (d1 + d2);
    }

    Quadrilateral::Quadrilateral(double base, double height) {
        d1 = d3 = base;
        d2 = d4 = height;
        p = (d1 + d2);

    }

    Quadrilateral::Quadrilateral(double edge) {
        d1 = d2 = d3 = d4 = edge;
        p = 2 * edge;
    }

    double Quadrilateral::Area() const {
        return (int) sqrt((p - d1) * (p - d2) * (p - d3) * (p - d4));
    }


    Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p3) : Quadrilateral(p1, p2, p3){
    }



    Rectangle::Rectangle(const Point &p1, const int &base, const int &height) : Quadrilateral(base, height) {

    }


    Square::Square(const Point &p1, const int &edge) : Quadrilateral(edge){

    }


}
