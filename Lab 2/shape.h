#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <math.h>

using namespace std;


namespace ShapeLibrary {

    class Point {
    private:
        double _x;
        double _y;
    public:
        Point(const double &x,
              const double &y) : _x(x), _y(y) {};

        Point(const Point &point);

        static double distance(const Point &p1,
                               const Point &p2) {
            double d;
            d = sqrt(pow((p2._x - p1._x), 2) + pow((p2._y - p1._y), 2));
            return d;
        };
    };

    class IPolygon {

    public:
        virtual double Area() const = 0;
    };

    class Ellipse : public IPolygon {
    private:
        Point _center;
        const int _a;
        const int _b;
    public:
        Ellipse(const Point &center,
                const int &a,
                const int &b) : _center(center), _a(a), _b(b) {};

        double Area() const;
    };

    class Circle : public Ellipse {
    public:
        Circle(const Point &center,
               const int &radius);
    };


    class Triangle : public IPolygon {
    public:
        double s;
        double a, b, c;

        Triangle(const Point &p1,
                 const Point &p2,
                 const Point &p3);
        Triangle(double a);

        double Area() const;
    };


    class TriangleEquilateral : public Triangle {
    public:
        TriangleEquilateral(const Point &p1,
                            const int &edge);
    };

    class Quadrilateral : public IPolygon {
    public:
        double p;
        double d1, d2, d3, d4;

        Quadrilateral(const Point &p1,
                      const Point &p2,
                      const Point &p3,
                      const Point &p4);
        Quadrilateral(const Point &p1,
                      const Point &p2,
                      const Point &p3);
        Quadrilateral(double base,
                      double height);
        Quadrilateral(double edge);

        double Area() const;
    };


    class Parallelogram : public Quadrilateral {
    public:
        double a, b;
        double s;

        Parallelogram(const Point &p1,
                      const Point &p2,
                      const Point &p3);

    };

    class Rectangle : public Quadrilateral {
    public:
        Rectangle(const Point &p1,
                  const int &base,
                  const int &height);
    };

    class Square : public Quadrilateral {
    public:
        Square(const Point &p1,
               const int &edge);
    };
}

#endif // SHAPE_H
