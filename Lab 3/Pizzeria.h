#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

namespace PizzeriaLibrary {

    class Ingredient {
    public:
        string Name;
        int Price;
        string Description;

        Ingredient(const string &name, const int &price, const string &description);
    };

    class Pizza {
    public:
        Pizza(const string &name, const vector<Ingredient> &ingredient);

        string Name;
        vector<Ingredient> ingredients;

        void AddIngredient(const Ingredient &ingredient);

        int NumIngredients() const { return ingredients.size(); }

        static unsigned int ComputePrice(const Pizza &);

    };

    class Order {
    public:
        Order(const vector<Pizza> &pizzas);

        vector<Pizza> pizzas;
        int id;
        static int count;

        void InitializeOrder(int numPizzas) { pizzas.reserve(numPizzas); }

        void AddPizza(const Pizza &pizza) { pizzas.push_back(pizza); }

        const Pizza &GetPizza(const int &position) const;

        int NumPizzas() const { return pizzas.size(); }

        unsigned int ComputeTotal() const;
    };

    class Pizzeria {
    public:
        vector<Ingredient> ingredients;
        vector<Pizza> menu;
        vector<Order> orders;

        void AddIngredient(const string &name,
                           const string &description,
                           const int &price);

        const Ingredient &FindIngredient(const string &name) const;

        void AddPizza(const string &name,
                      const vector<string> &stringIngredients);

        const Pizza &FindPizza(const string &name) const;

        int CreateOrder(const vector<string> &pizzas);

        const Order &FindOrder(const int &numOrder) const;

        string GetReceipt(const int &numOrder) const;

        string ListIngredients() const;

        string Menu() const;

    };
};

#endif // PIZZERIA_H
