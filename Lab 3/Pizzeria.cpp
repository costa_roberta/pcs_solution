#include "Pizzeria.h"


namespace PizzeriaLibrary {
    int Order::count = 1000;

    Ingredient::Ingredient(const string &name, const int &price, const string &description) {
        this->Name = name;
        this->Price = price;
        this->Description = description;
    }

    Pizza::Pizza(const string &name, const vector<Ingredient> &ingredient) {
        this->Name = name;
        this->ingredients = ingredient;
    }

    void Pizza::AddIngredient(const Ingredient &ingredient) {
        unsigned int reinforcement = 0;
        //Iterating to find more than 2 reinforcements
        for (auto &ing : ingredients) {
            if (reinforcement <= 1) {
                if (ing.Name == ingredient.Name) reinforcement++;
            } else
                throw runtime_error("Ingredient already inserted"); //Throw exception in case of reinforcements >= 2
        }
        ingredients.push_back(ingredient);
    }

    unsigned int Pizza::ComputePrice(const Pizza &pizza) {
        unsigned int price = 0;
        for (auto &ing : pizza.ingredients) price += ing.Price;
        return price;
    }


    unsigned int Order::ComputeTotal() const {
        unsigned int total = 0;
        for (auto pizza : pizzas) total += Pizza::ComputePrice(pizza);
        return total;
    }

    const Pizza &Order::GetPizza(const int &position) const {
        if (position <= pizzas.size()) return pizzas[position];
        else throw runtime_error("Position passed is wrong");
    }

    Order::Order(const vector<Pizza> &pizzas) {
        this->pizzas = pizzas;
        this->id = count++;
    }

    void Pizzeria::AddIngredient(const string &name, const string &description, const int &price) {
        for (auto &ingredient : ingredients) {
            if (ingredient.Name == name)
                throw runtime_error("Ingredient already inserted");
        }
        ingredients.emplace_back(name, price, description);
    }

    const Ingredient &Pizzeria::FindIngredient(const string &name) const {
        for (unsigned int i = 0; i < ingredients.size(); i++) {
            if (ingredients[i].Name == name) {
                return ingredients[i];
            }
        }
        throw runtime_error("Ingredient not found");
    };

    void Pizzeria::AddPizza(const string &name, const vector<string> &stringIngredients) {
        for (auto &pizza : menu) {
            if (pizza.Name == name)
                throw runtime_error("Pizza already inserted");
        }
        vector<Ingredient> ings;
        for (auto &strIng : stringIngredients) {
            for (auto &ing : ingredients) {
                if (ing.Name == strIng) {
                    ings.push_back(ing);
                    break;
                }
            }
        }
        menu.emplace_back(name, ings);
    };

    const Pizza &Pizzeria::FindPizza(const string &name) const {
        for (unsigned int i = 0; i < menu.size(); i++) {
            if (menu[i].Name == name) {
                return menu[i];
            }
        }
        throw runtime_error("Pizza not found");
    }

    int Pizzeria::CreateOrder(const vector<string> &strPizzas) {
        if (strPizzas.empty()) throw runtime_error("Empty order");
        vector<Pizza> pizzas;
        for(auto & npizza : strPizzas){
            for(auto & pippo : menu){
                if(npizza == pippo.Name){
                    pizzas.push_back(pippo);
                    break;
                }
            }
        }
        Order order(pizzas);
        orders.push_back(order);
        return order.id;
    }

    const Order &Pizzeria::FindOrder(const int &numOrder) const {
        for (unsigned int i = 0; i < orders.size(); i++) {
            if (orders[i].id == numOrder) {
                return orders[i];
            }
        }
        throw runtime_error("Order not found");
    }

    string Pizzeria::GetReceipt(const int &numOrder) const {
        Order order = FindOrder(numOrder);
        string receipt;
        for (auto &pizza : order.pizzas) {
            receipt += "- " + pizza.Name + ", " + to_string(Pizza::ComputePrice(pizza)) + " euro" + "\n";
        }
        receipt += " TOTAL: " + to_string(order.ComputeTotal()) + " euro" + "\n";
        return receipt;
    }

    string Pizzeria::ListIngredients() const {
        string list;
        for (auto &ingredient : ingredients) {
            list += ingredient.Name + " - '" + ingredient.Description + "': " + to_string(ingredient.Price) + " euro" +
                    "\n";
        }
        return list;
    }

    string Pizzeria::Menu() const {
        string menuString;
        for (auto &pizza : menu) {
            menuString += pizza.Name +
                          " (" + to_string(pizza.NumIngredients()) + " ingredients): "
                          + to_string(Pizza::ComputePrice(const_cast<Pizza &>(pizza))) + " euro" + "\n";
        }
        return menuString;
    }


}
