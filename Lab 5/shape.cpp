#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

    Point::Point(const double &x, const double &y) {
        this->X = x;
        this->Y = y;

    }

    Ellipse::Ellipse(const Point &center, const double &a, const double &b) {
        _center = center;
        _a = a;
        _b = b;
    }
    double Ellipse::Perimeter() const {
        double perimeter = 2 * M_PI * sqrt((pow(_a, 2) + pow(_b, 2))/2);
        return perimeter;
    }

    Circle::Circle(const Point &center, const double &radius) {
        _center = center;
        _a = _b = radius;
    }

    Triangle::Triangle(const Point &p1,
                       const Point &p2,
                       const Point &p3) {
        points[0] = p1 - p2;
        points[1] = p2 - p3;
        points[2] = p3 - p1;
        a = points[0].ComputeNorm2();
        b = points[1].ComputeNorm2();
        c = points[2].ComputeNorm2();

    }

    double Triangle::Perimeter() const {
        double perimeter = 0;
        perimeter = a + b + c;
        return perimeter;
    }

    TriangleEquilateral::TriangleEquilateral(const Point &p1,
                                             const double &edge) {
      a = b = c = edge;
    }


    Quadrilateral::Quadrilateral(const Point &p1,
                                 const Point &p2,
                                 const Point &p3,
                                 const Point &p4) {
        points[0] = p1 - p2;
        points[1] = p2 - p3;
        points[2] = p3 - p4;
        points[3] = p4 - p1;
        a = points[0].ComputeNorm2();
        b = points[1].ComputeNorm2();
        c = points[2].ComputeNorm2();
        d = points[3].ComputeNorm2();

    }

    double Quadrilateral::Perimeter() const {
        double perimeter = 0;
        perimeter = a + b + c + d;
        return perimeter;
    }

    Rectangle::Rectangle(const Point &p1,
                         const double &base,
                         const double &height) {
        a = c = base;
        b = d = height;
    }

    Square::Square(const Point &p1, const double &edge) {
        a = b = c = d = edge;
    }

    Point Point::operator+(const Point &point) const{
        Point tempPoint(X, Y);
        tempPoint.X = X + (point.X);
        tempPoint.Y = Y + (point.Y);
        return tempPoint;

    }

    Point Point::operator-(const Point &point) const {
        Point tempPoint(X, Y);
        tempPoint.X = X - (point.X);
        tempPoint.Y = Y - (point.Y);
        return tempPoint;

    }

    Point &Point::operator-=(const Point &point){
        X -= point.X;
        Y -= point.Y;
        return *this;
    }

    Point &Point::operator+=(const Point &point) {
        X += point.X;
        Y += point.Y;
        return *this;

    }

}
