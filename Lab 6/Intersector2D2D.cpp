#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-5;
    toleranceIntersection = 1.0E-7;

}


Intersector2D2D::~Intersector2D2D()
{

}

// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    _planeNormal1 = planeNormal;
    _planeTranslation1 = planeTranslation;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    _planeNormal2 = planeNormal;
    _planeTranslation2 = planeTranslation;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    Vector3d n(0,0,0);
    if (_planeNormal1.cross(_planeNormal2)== n)
    {
        if (_planeTranslation1 == _planeTranslation2)
        {
            intersectionType = Coplanar;
            return false;
        }
        else
        {
            intersectionType = NoInteresection;
            return false;
        }
    }
    else
    {
        intersectionType = LineIntersection;
        return true;
    }
}