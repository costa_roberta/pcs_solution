#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D() {
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
}

Intersector2D1D::~Intersector2D1D() {

}

Vector3d Intersector2D1D::IntersectionPoint() {
    t = -(((_lineTangent.transpose()(0, 0)) * ((_planeTranslation * _planeNormal.transpose()(0, 0)) + _lineOrigin[0])) +
          ((_lineTangent.transpose()(0, 1)) * ((_planeTranslation * _planeNormal.transpose()(0, 1)) + _lineOrigin[1])) +
          ((_lineTangent.transpose()(0, 2)) * ((_planeTranslation * _planeNormal.transpose()(0, 2)) + _lineOrigin[2])));
    Vector3d pointIntersection(_lineOrigin[0] + (_lineTangent.transpose()(0, 0) * t),
                               _lineOrigin[1] + (_lineTangent.transpose()(0, 1) * t),
                               _lineOrigin[2] + (_lineTangent.transpose()(0, 2) * t));

    return pointIntersection;
}

// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d &planeNormal, const double &planeTranslation) {
    _planeNormal = planeNormal;
    _planeTranslation = planeTranslation;
}

// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d &lineOrigin, const Vector3d &lineTangent) {
    _lineOrigin = lineOrigin;
    _lineTangent = lineTangent;
}

// ***************************************************************************
bool Intersector2D1D::ComputeIntersection() {
    cond1 = _planeNormal.dot(_lineTangent);
    cond2 = _lineOrigin.dot(_planeNormal) + _planeTranslation;

    if (cond1 == 0 && cond2 == 0) {
        intersectionType = Coplanar;
    } else if (cond1 != 0) {
        intersectionType = PointIntersection;
    } else if (cond1 == 0 && cond2 != 0) {
        intersectionType = NoInteresection;
    }
    return true;
}
