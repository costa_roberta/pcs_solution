#ifndef POLYGONCUT_H
#define POLYGONCUT_H

#include <iostream>
#include <vector>
#include <math.h>
#include <Eigen>

#include "polygon.hpp"

using namespace std;
using namespace PolygonLibrary;

namespace PolygonCutLibrary {

  class Intersection : public IIntersection {
    //compute the intersection

  };

  class Polygon : public IPolygon{
    //father class

  };

  class Delete : public IDelete {
    //return new polygon

  };
}
#endif // POLYGONCUT_H
