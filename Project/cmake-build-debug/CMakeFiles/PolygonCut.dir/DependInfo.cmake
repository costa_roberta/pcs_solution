# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/Roberta/Desktop/Anno II/Programmazione e calcolo scientifico/PCS_solution_C++/Project/googletest/googlemock/src/gmock-all.cc" "D:/Roberta/Desktop/Anno II/Programmazione e calcolo scientifico/PCS_solution_C++/Project/cmake-build-debug/CMakeFiles/PolygonCut.dir/googletest/googlemock/src/gmock-all.cc.obj"
  "D:/Roberta/Desktop/Anno II/Programmazione e calcolo scientifico/PCS_solution_C++/Project/googletest/googletest/src/gtest-all.cc" "D:/Roberta/Desktop/Anno II/Programmazione e calcolo scientifico/PCS_solution_C++/Project/cmake-build-debug/CMakeFiles/PolygonCut.dir/googletest/googletest/src/gtest-all.cc.obj"
  "D:/Roberta/Desktop/Anno II/Programmazione e calcolo scientifico/PCS_solution_C++/Project/main.cpp" "D:/Roberta/Desktop/Anno II/Programmazione e calcolo scientifico/PCS_solution_C++/Project/cmake-build-debug/CMakeFiles/PolygonCut.dir/main.cpp.obj"
  "D:/Roberta/Desktop/Anno II/Programmazione e calcolo scientifico/PCS_solution_C++/Project/src/PolygonCut.cpp" "D:/Roberta/Desktop/Anno II/Programmazione e calcolo scientifico/PCS_solution_C++/Project/cmake-build-debug/CMakeFiles/PolygonCut.dir/src/PolygonCut.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_LANGUAGE_CXX11"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../googletest/googletest"
  "../googletest/googletest/include"
  "../googletest/googlemock"
  "../googletest/googlemock/include"
  "../src"
  "../test"
  "../eigen/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
